const Page = require('./../pageobjects/page')

describe('Open application', () => {
  it('log should be visible', async () => {
    await Page.open()

    const elem = await $('#myButton')
    await expect(elem).toBeDisplayed()
  })
  it('First Press', async () => {
    await Page.open()
    await Page.pressButton()

    const elements = await $$('#log')
    await expect(elements).toHaveChildren({ gte: 1 })
  })
  it('Second Press', async () => {
    await Page.open()
    await Page.pressButton()


    const elements = await $$('#log')
    await expect(elements).toHaveChildren({ gte: 1 })
  })
  // keep code coverage at 100%
  it('plenty of Press', async () => {
    await Page.open()
    await Page.pressButton()

    const elements = await $$('#log')
    for (let index = 1; index < 50; index++) {
      Page.pressButton()
      await expect(elements).toHaveChildren({ gte: index })
    }
  })
})
